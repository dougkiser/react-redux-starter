import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import reduxThunkMiddleware from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import reducers from './reducers';

const history = createHistory();

const finalReducer = combineReducers(Object.assign({}, reducers, {router: routerReducer}));

const composeEnhancers =
  process.env.NODE_ENV === 'development' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(
    reduxThunkMiddleware,
    routerMiddleware(history)
  )
);

export default initialState => createStore(
  finalReducer,
  initialState,
  enhancer
);

export {
  history
};
