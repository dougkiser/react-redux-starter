import { defaultSelector } from '../../store/selectors';
import { merge } from 'ramda';

const home = state => merge({
  count: state.counter.get('count')
}, defaultSelector(state), {});

export default home;