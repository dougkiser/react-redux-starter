import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'ramda';
import selector from './home.selector';
import { Button } from 'semantic-ui-react';
import * as HomeActions from './home.actions';
import * as CommonActions from '../../store/actions';
import { WithMenu } from '../../hoc';

const Home = props => (
  <div className="home">
    <Button color="red"
            onClick={() => props.increment()}>
      Increment
    </Button>
    <Button color="green"
            onClick={() => props.decrement()}>
      Decrement
    </Button>
    {props.count}
  </div>
);

Home.propTypes = {
  increment: PropTypes.func,
  decrement: PropTypes.func
};

export {
  Home
};

export default connect(selector, {
  ...CommonActions,
  ...HomeActions
})(
  compose(
    WithMenu
  )(Home)
);
