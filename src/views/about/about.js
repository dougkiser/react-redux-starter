import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'ramda';
import { WithMenu } from '../../hoc';
import * as CommonActions from '../../store/actions';

const About = props => (
  <div className="about">
    <h1>About</h1>
  </div>
);

export {
  About
};

export default connect(state => ({}), {
  ...CommonActions
})(
  compose(
    WithMenu
  )(About)
);