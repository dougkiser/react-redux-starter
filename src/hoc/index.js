import WithState from './with-state/index';
import WithMenu from './with-menu';

export {
  WithState,
  WithMenu
};
