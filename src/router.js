import React from 'react';
import { Route } from 'react-router';
import { Home, About } from './views';

export default props => (
  <div className="router">
    <Route exact
           path="/"
           component={Home}/>
    <Route path="/home"
           component={Home}/>
    <Route path="/about"
           component={About}/>
  </div>
);
