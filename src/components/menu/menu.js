import React from 'react';
import PropTypes from 'prop-types';
import {Menu} from 'semantic-ui-react';
import {map} from 'ramda';

const StatelessMenu = ({navigate, history}) => {
  const pathName = history.location.pathname;
  const items = map(item => (
    <Menu.Item {...item}/>
  ), [{
    name: 'React-Redux-Starter',
    onClick: e => navigate('/home'),
    key: 1
  }, {
    name: 'home',
    active: pathName === '/home',
    onClick: () => navigate('/home'),
    key: 2
  }, {
    name: 'about',
    active: pathName === '/about',
    onClick: () => navigate('/about'),
    key: 3
  }]);

  return (
    <Menu>
      {items}
    </Menu>
  );
};

StatelessMenu.propTypes = {
  activeItem: PropTypes.string,
  setActiveItem: PropTypes.func
};

export default StatelessMenu;